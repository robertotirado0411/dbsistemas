/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;
import com.sun.imageio.plugins.jpeg.JPEG;
import java.awt.event.ActionEvent;
import vista.jifProductos;
import modelo.*;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Edgar Guerrero
 */
public class Controlador implements ActionListener{
    
    private jifProductos vista;
    private dbProducto db;
    private boolean EsActualizar;
    private int idProducto;

    public Controlador(jifProductos vista, dbProducto db) {
        this.vista = vista;
        this.db = db;
        
        vista.btnCancelar.addActionListener(this);
        vista.btnCerrar.addActionListener(this);
        vista.btnDeshabilitar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnLimpiar.addActionListener(this);
        vista.btnNuevo.addActionListener(this);
        vista.btnGuardar.addActionListener(this);
        vista.btnHabilitar.addActionListener(this);

        
        this.deshabilitar();
    }
    
    public void limpiar(){
        vista.txtCodigo.setText("");
        vista.txtNombre.setText("");
        vista.txtPrecio.setText("");
        vista.dtfFecha.setDate(null);
    }
    
    public void cerrar(){
        
        int res = JOptionPane.showConfirmDialog(vista,"Desea cerrar el sistema?","Productos",JOptionPane.YES_OPTION,JOptionPane.QUESTION_MESSAGE);
        
        if(res == JOptionPane.YES_OPTION){
            vista.dispose();
        }
        
    }
    
    public void habilitar(){
        vista.txtCodigo.setEnabled(true);
        vista.txtNombre.setEnabled(true);
        vista.txtPrecio.setEnabled(true);
        vista.btnGuardar.setEnabled(true);
        vista.btnDeshabilitar.setEnabled(true);
        vista.btnBuscar.setEnabled(true);
        vista.btnHabilitar.setEnabled(true);
        vista.dtfFecha.setEnabled(true);


        
    }
    
    public void deshabilitar(){
        vista.txtCodigo.setEnabled(!true);
        vista.txtNombre.setEnabled(!true);
        vista.txtPrecio.setEnabled(!true);
        vista.btnDeshabilitar.setEnabled(!true);
        vista.btnGuardar.setEnabled(!true);
        vista.btnBuscar.setEnabled(!true);
        vista.btnHabilitar.setEnabled(false);
        vista.dtfFecha.setEnabled(false);

        
    }
    
    public boolean validar (){
        boolean exito=false;
        
        if (vista.txtCodigo.getText().equals("")
                || vista.txtNombre.getText().equals("")
                || vista.dtfFecha.getDate() == null
                || vista.txtPrecio.getText().equals("")) {
            exito = true;
        }

        return exito;

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource()==vista.btnLimpiar){ this.limpiar();}
        if(ae.getSource()==vista.btnCancelar){ this.limpiar(); this.deshabilitar();}
        if(ae.getSource()==vista.btnCerrar){ this.cerrar();}
        
        if(ae.getSource()==vista.btnNuevo){this.habilitar(); this.EsActualizar=false;}
        
        if(ae.getSource()==vista.btnGuardar){
            
            //validar
            if(this.validar()==false){
                
                Productos pro = new Productos();
                pro.setCodigo(vista.txtCodigo.getText());
                pro.setNombre(vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(vista.txtPrecio.getText()));
                pro.setStatus(0);
                pro.setFecha(this.convertirAñoMesDia(vista.dtfFecha.getDate()));
                
                try{
                if(this.EsActualizar == false){
                        if (db.siExiste(pro.getCodigo())) {
                            JOptionPane.showMessageDialog(vista, "El codigo ya existe. Por favor, use un codigo diferente.");
                            vista.txtCodigo.requestFocusInWindow();
                        }else{
                    db.insertar(pro);
                    JOptionPane.showMessageDialog(vista, "Se agregó con exito el producto");
                    this.limpiar();
                    this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                }             

                } else{
                    db.actualizar(pro);
                    JOptionPane.showMessageDialog(vista, "Se actualizó con exito el producto");
                    this.limpiar(); this.deshabilitar();
                    this.ActualizarTabla(db.lista());
                }
                
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgió un error" +e.getMessage());
                }
                
            }else JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
            
        }
        
        if(ae.getSource()==vista.btnBuscar){
            Productos pro = new Productos();
            
            if(vista.txtCodigo.getText().equals("")){
                JOptionPane.showMessageDialog(vista, "Faltaron datos por capturar");
            }
            else{
                
                try{
                    
                    pro  = (Productos) db.buscar(vista.txtCodigo.getText());
                    if(pro.getIdProductos()!=0){
                        idProducto = pro.getIdProductos();
                        
                        vista.txtNombre.setText(pro.getNombre());
                        vista.txtPrecio.setText(String.valueOf(pro.getPrecio()));
                        convertirStringDate(pro.getFecha());
                        this.EsActualizar=true;
                        vista.btnDeshabilitar.setEnabled(true);
                        vista.btnGuardar.setEnabled(true);
                    } 
                } catch(Exception e){
                    JOptionPane.showMessageDialog(vista, "Surgió un error" +e.getMessage());
                }
                
            }
            
        }
        

        if (ae.getSource() == vista.btnDeshabilitar) {

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vista, "Desea deshabilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    try {
                        db.deshabilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto fue deshabilitado ");
                        this.limpiar();
                        this.ActualizarTabla(db.lista());
                        vista.txtCodigo.requestFocusInWindow();

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                    }
                }
            }

        }

        if (ae.getSource() == vista.btnHabilitar) {

            if (vista.txtCodigo.getText().equals("")) {
                JOptionPane.showMessageDialog(vista, "Ingrese el codigo");
            } else {
                int opc = 0;
                opc = JOptionPane.showConfirmDialog(vista, "Desea habilitar el producto? ", "Producto",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (opc == JOptionPane.YES_OPTION) {
                    Productos pro = new Productos();
                    pro.setCodigo(vista.txtCodigo.getText());
                    try {
                        db.habilitar(pro);
                        JOptionPane.showMessageDialog(vista, "El producto fue habilitado ");
                        this.limpiar();
                        this.ActualizarTabla(db.lista());
                        vista.txtCodigo.requestFocusInWindow();
                        // ACTUALIZAR LA TABLA

                    } catch (Exception i) {
                        JOptionPane.showMessageDialog(vista, "Surgio un error:  " + i.getMessage());
                    }
                }
            }

        }

        if (ae.getSource() == vista.btnLimpiar) {
            this.limpiar();
        }
        if (ae.getSource() == vista.btnCancelar) {
            this.limpiar();
            this.deshabilitar();
        }
        if (ae.getSource() == vista.btnCerrar) {
            int res = JOptionPane.showConfirmDialog(vista, "Deseas cerrar el sistema", "Productos", JOptionPane.YES_OPTION, JOptionPane.QUESTION_MESSAGE);

            if (res == JOptionPane.YES_OPTION) {
                vista.dispose();
            }
        }
        
        
    }
    
    public void iniciarVista(){
            vista.setTitle("Productos");
            vista.resize(1000,800);
            vista.setVisible(true);            
            try{
              this.ActualizarTabla(db.lista());
              
            }catch (Exception e){
                JOptionPane.showMessageDialog(vista, "Surgio un error"+ e.getMessage());
            }
    }
    
    public String convertirAñoMesDia(Date fecha){
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(fecha);
        
    }
    
    public void convertirStringDate(String fecha){
        
        try{
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
            Date date = dateFormat.parse(fecha);
            vista.dtfFecha.setDate(date);
        } catch(ParseException e){
            System.err.print(e.getMessage());
        }
        
    }
    
    public void ActualizarTabla (ArrayList<Productos>arr){
        String campos[]={"idProducto","codigo","nombre","precio","fecha"};
        String[][] datos = new String [arr.size()][5];
        int renglon=0;
        for (Productos registro : arr){
            datos[renglon][0]=String.valueOf(registro.getIdProductos());
            datos[renglon][1]=registro.getCodigo();
            datos[renglon][2]=registro.getNombre();
            datos[renglon][3]=String.valueOf(registro.getPrecio());
            datos[renglon][4]=registro.getFecha();
            
         renglon++;   
        }
        DefaultTableModel tb= new DefaultTableModel(datos,campos);
        vista.lista.setModel(tb);
    }
    
}
